import React from "react";

class NumField extends React.Component {
  constructor(props) {
    super(props);
    this.selectField = this.selectField.bind(this);
  }
  selectField() {
    if (!this.props.isDisabled) {
      this.props.onSelection();
    }
  }
  render() {
    return (
      <div
        className={`numField ${this.props.selected ? "selected" : ""} ${
          this.props.isDisabled ? "disabled" : ""
        }`}
        onClick={this.selectField}
      >
        <div className="number">{this.props.number}</div>
        {/* {this.props.isDisabled} */}
      </div>
    );
  }
}

export default NumField;
