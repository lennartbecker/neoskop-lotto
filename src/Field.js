import React from "react";
import NumField from "./NumField";
class Field extends React.Component {
  constructor(props) {
    super();
    this.state = {
      fields: [],
      showResults: false,
    };
    this.state.fields = this.generateFields();
    this.onSelection = this.onSelection.bind(this);
    this.clearField = this.clearField.bind(this);
    this.showResults = this.showResults.bind(this);
  }
  render() {
    return (
      <div className="wrapper">
        <div className={`fieldResults ${this.state.showResults ? "show" : ""}`}>
          Ihre Auswahl:
          <div className="results">{this.getResults()}</div>
        </div>
        <div className="fieldHeader">
          <h6>Ihr Spielfeld</h6>
          <h6>1 von 12</h6>
        </div>
        <div className="field">{this.generateDomFields()}</div>
        <div className="fieldControls">
          <button className="del" onClick={this.clearField}>
            Löschen
          </button>
          <button
            className={`btn next ${this.showNextButton() ? "show" : ""}`}
            onClick={this.showResults}
          >
            Weiter
          </button>
        </div>
      </div>
    );
  }
  onSelection(number) {
    let index = this.state.fields.map((field) => field.number).indexOf(number);
    let fieldsCopy = [...this.state.fields];
    fieldsCopy[index].selected = !fieldsCopy[index].selected;
    this.setState({
      fields: fieldsCopy,
      showResults: false,
    });
  }
  clearField() {
    this.setState({
      fields: this.generateFields(),
      showResults: false,
    });
  }
  isDisabled(number) {
    let selectedFields = this.state.fields.filter((field) => field.selected);
    return (
      selectedFields.length === 6 &&
      selectedFields.map((field) => field.number).indexOf(number) === -1
    );
  }
  generateDomFields() {
    let newfields = [];
    this.state.fields.forEach((field) => {
      newfields.push(
        <NumField
          number={field.number}
          key={field.number}
          selected={field.selected}
          onSelection={() => this.onSelection(field.number)}
          isDisabled={this.isDisabled(field.number)}
        />
      );
    });
    return newfields;
  }
  generateFields() {
    let fields = [];
    for (let i = 1; i <= 49; i++) {
      fields.push({
        selected: false,
        number: i,
      });
    }
    return fields;
  }
  showNextButton() {
    return this.state.fields.filter((field) => field.selected).length === 6;
  }
  showResults() {
    this.setState({
      showResults: !this.state.showResults,
    });
  }
  getResults() {
    let results = this.state.fields.filter((field) => field.selected);
    console.log(results)
    return results.map((result) => (
      <div className="selectedField">{result.number}</div>
    ));
  }
}

export default Field;
